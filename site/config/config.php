<?php
return [
    'api' => [
        'basicAuth' => true,
        'allowInsecure' => true
    ],
    'debug' => true
];