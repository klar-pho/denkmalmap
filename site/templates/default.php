<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Denkmal</title>
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no" />
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />
    <script src="https://unpkg.com/@mapbox/mapbox-sdk/umd/mapbox-sdk.min.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <style>
        body { margin: 0; padding: 0; }
        #map { position: absolute; top: 0; bottom: 0; width: 100%; }
    </style>
</head>
<body>
<div class="wrapper">
    <div class='sidebar'>
        <div class='heading'>
            <h1>Fachpersonensuche</h1>
            <a href="#" class="go-center" id="center">Zurück zur Übersicht</a>
            <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Nach Personen suchen" title="Suchen...">
        </div>
        <div id='listings' class='listings'></div>
    </div>
    <div id='map' class='map'></div>
</div>
<nav id="filter-group" class="filter-group"></nav>
<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <p>Some text in the Modal..</p>
    </div>

</div>
<script src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
<script src="assets/js/map.js"></script>
<script src="assets/js/phptest.js"></script>
</body>
</html>