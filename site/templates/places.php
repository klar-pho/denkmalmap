<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>One</title>
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no" />
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />
    <script src="https://unpkg.com/@mapbox/mapbox-sdk/umd/mapbox-sdk.min.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <style>
        body { margin: 0; padding: 0; }
        #map { position: absolute; top: 0; bottom: 0; width: 100%; }
    </style>
</head>
<body>
<div class='sidebar'>
    <div class='heading'>
        <h1>Wow, Much Denkmal </h1>
    </div>
    <div id='listings' class='listings'></div>
</div>
<div id='map' class='map'></div>
<script src="assets/js/map.js"></script>
<script src="assets/js/phptest.js"></script>
</body>
</html>