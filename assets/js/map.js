// This will let you use the .remove() function later on
if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function () {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}

mapboxgl.accessToken = 'pk.eyJ1IjoicGF0cmlja2F0a2xhciIsImEiOiJjazk5eDViNzgwMmZiM2dueWI4MTRzcjNxIn0.iwVL-oucgXJxwIHAaUceKA';
/**
 * Add the map to the page
 */
var filterGroup = document.getElementById('filter-group');
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/light-v10',
    center: [8.543897, 46.618113],
    zoom: 7,
    scrollZoom: true
});

var stores = {
    "type": "FeatureCollection",
    "features": []
};

fetch('https://patrickhofer.ch/klar2020/maps/places.json').then(function (response) {
    return response.json();
}).then(function (data) {

    data.forEach(function (elem) {
        stores.features.push(
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        elem.longitude,
                        elem.latitude
                    ]
                },
                "properties": {
                    "heading": elem.heading,
                    "placename": elem.placename,
                    "description": elem.description,
                    "name": elem.name,
                    "contact": elem.kontakt,
                    "category": elem.category,
                    "work": elem.arbeitsschwerpunkte,
                    "image": elem.image[0],
                    "id": Math.random()
                }
            });
    })

    stores.features.forEach(function (store, i) {
        store.properties.id = i;
    });

}).catch(function () {
    console.log("Booo");
});


/**
 * Assign a unique id to each store. You'll use this `id`
 * later to associate each point on the map with a listing
 * in the sidebar.
 */

/**
 * Wait until the map loads to make changes to the map.
 */
map.on('load', function (e) {
    /**
     * This is where your '.addLayer()' used to be, instead
     * add only the source without styling a layer
     */
    map.addSource("places", {
        "type": "geojson",
        "data": stores
    });

    stores.features.forEach(function (feature) {
        var symbol = feature.properties['category'];
        var layerID = 'listing-' + symbol.toLowerCase().replace(/\s/g, '');
        var markerID = 'marker-' + symbol.toLowerCase().replace(/\s/g, '');


// Add checkbox and label elements for the layer.
        var input = document.createElement('input');
        input.type = 'checkbox';
        input.id = layerID;
        input.checked = true;
        filterGroup.appendChild(input);

        var label = document.createElement('label');
        label.setAttribute('for', layerID);
        label.textContent = symbol;
        filterGroup.appendChild(label);

// When the checkbox changes, update the visibility of the layer.
        input.addEventListener('change', function (e) {
            let allbylayer = document.getElementsByClassName(layerID);
            let allbyMarkerlayer = document.getElementsByClassName(markerID);


            if (e.target.checked) {
                for (var i = 0; i < allbylayer.length; i++) {
                    allbylayer[i].style.display = 'block';
                }
                for (var i = 0; i < allbyMarkerlayer.length; i++) {
                    allbyMarkerlayer[i].style.display = 'block';
                }
            } else {
                for (var i = 0; i < allbylayer.length; i++) {
                    allbylayer[i].style.display = 'none';
                }
                for (var i = 0; i < allbyMarkerlayer.length; i++) {
                    allbyMarkerlayer[i].style.display = 'none';
                }
            }
        });
    });

    /**
     * Add all the things to the page:
     * - The location listings on the side of the page
     * - The markers onto the map
     */
    buildLocationList(stores);
    addMarkers();
});

/**
 * Add a marker to the map for every store listing.
 **/
function addMarkers() {
    /* For each feature in the GeoJSON object above: */
    stores.features.forEach(function (marker) {
        /* Create a div element for the marker. */
        var el = document.createElement('div');
        /* Assign a unique `id` to the marker. */
        el.id = "marker-" + marker.properties.id;
        /* Assign the `marker` class to each marker for styling. */
        el.className = 'marker' + ' marker-' + marker.properties.category.toLowerCase().replace(/\s/g, '');;

        /**
         * Create a marker using the div element
         * defined above and add it to the map.
         **/
        new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)
            .addTo(map);

        /**
         * Listen to the element and when it is clicked, do three things:
         * 1. Fly to the point
         * 2. Close all other popups and display popup for clicked store
         * 3. Highlight listing in sidebar (and remove highlight for all other listings)
         **/
        el.addEventListener('click', function (e) {
            /* Fly to the point */
            flyToStore(marker);
            /* Close all other popups and display popup for clicked store */
            createPopUp(marker);
            /* Highlight listing in sidebar */
            var activeItem = document.getElementsByClassName('active');
            e.stopPropagation();
            if (activeItem[0]) {
                activeItem[0].classList.remove('active');
            }
            var listing = document.getElementById('listing-' + marker.properties.id);
            listing.classList.add('active');
        });
    });
}

/**
 * Add a listing for each store to the sidebar.
 **/
function buildLocationList(data) {
    data.features.forEach(function (store, i) {
        /**
         * Create a shortcut for `store.properties`,
         * which will be used several times below.
         **/
        var prop = store.properties;

        /* Add a new listing section to the sidebar. */
        var listings = document.getElementById('listings');
        var listing = listings.appendChild(document.createElement('div'));
        /* Assign a unique `id` to the listing. */
        listing.id = "listing-" + prop.id;
        listing.category = "listing-" + prop.category;
        /* Assign the `item` class to each listing for styling. */
        listing.className = 'item ' + 'listing-' + prop.category.toLowerCase().replace(/\s/g, '');

        /* Add the link to the individual listing created above. */
        var link = listing.appendChild(document.createElement('a'));
        link.href = '#';
        link.className = 'title';
        link.id = "link-" + prop.id;

        link.innerHTML = prop.name + ' (' + prop.category + ')';

        /* Add details to the individual listing. */
        var details = listing.appendChild(document.createElement('div'));
        details.innerHTML = prop.heading;

        /* Add the Button to the individual listing created above. */
        var place = listing.appendChild(document.createElement('p'));
        place.className = 'place';
        place.innerHTML = prop.placename;

        /* Add the Button to the individual listing created above. */
        var button = listing.appendChild(document.createElement('a'));
        button.className = 'more';

        button.innerHTML = 'Mehr über ' + prop.name;

        var moreabout = listing.appendChild(document.createElement('div'));
        moreabout.className = 'more-about';


        if (prop.image) {
            var image = moreabout.appendChild(document.createElement('div'));
            image.className = 'person-image';
            image.innerHTML = `
            <img src="https://patrickhofer.ch/klar2020/maps/places/${prop.image}">

            `
        }


        var contact = moreabout.appendChild(document.createElement('p'));
        contact.className = 'contact';
        contact.innerHTML = `
            <h3>Kontakt</h3>
            <p>${prop.contact}</p>
            `



        $(button).click(function() {
            if ($(this).next().hasClass('open')) {
                $(this).next().removeClass('open')
            } else {
                $(this).next().addClass('open')
            }
        })



        /**
         * Listen to the element and when it is clicked, do four things:
         * 1. Update the `currentFeature` to the store associated with the clicked link
         * 2. Fly to the point
         * 3. Close all other popups and display popup for clicked store
         * 4. Highlight listing in sidebar (and remove highlight for all other listings)
         **/
        link.addEventListener('click', function (e) {
            for (var i = 0; i < data.features.length; i++) {
                if (this.id === "link-" + data.features[i].properties.id) {
                    var clickedListing = data.features[i];
                    flyToStore(clickedListing);
                    createPopUp(clickedListing);
                }
            }
            var activeItem = document.getElementsByClassName('active');
            if (activeItem[0]) {
                activeItem[0].classList.remove('active');
            }
            this.parentNode.classList.add('active');
        });
    });
}

/**
 * Use Mapbox GL JS's `flyTo` to move the camera smoothly
 * a given center point.
 **/
function flyToStore(currentFeature) {
    map.flyTo({
        center: currentFeature.geometry.coordinates,
        zoom: 12
    });
}

/**
 * Create a Mapbox GL JS `Popup`.
 **/
function createPopUp(currentFeature) {
    var popUps = document.getElementsByClassName('mapboxgl-popup');
    if (popUps[0]) popUps[0].remove();
    var popup = new mapboxgl.Popup({closeOnClick: true})
        .setLngLat(currentFeature.geometry.coordinates)
        .setHTML('<h3>' + currentFeature.properties.name + '</h3>' +
            '<h4>' + currentFeature.properties.heading + '</h4>')
        .addTo(map);

}


document.getElementById('center').addEventListener('click', function (e) {
    map.flyTo({
        center: [8.543897, 46.618113],
        zoom: 7
    });
});

function myFunction() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    li = document.getElementsByClassName("item");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
